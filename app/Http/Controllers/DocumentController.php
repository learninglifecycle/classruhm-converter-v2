<?php

namespace App\Http\Controllers;

use App\Media;
use App\MediaType;
use Config;
use CurlFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DocumentController extends MediaController
{
	public function convert($media)
	{
		// TODO: Provide different rules of the mimetype in a PowerPoint, so we can create HTML5 presentations.
		// Process the file with unoconv.
		$filepath = Config::get('media.temp_dir') . $media->getTempFilename();
		$destfilename = Config::get('media.content_dir') . $media->getDestFilename();
		$cmd = Config::get('media.unoconv_binary') . ' -f pdf -o "' . $destfilename . '" "' . $filepath . '" 2>&1';
		Log::info($cmd);
		exec($cmd, $output);
		Log::info($output);
		$dir = scandir(Config::get('media.content_dir'));
		Log::info(print_r($dir, true));

		if (file_exists($destfilename))
		{
			$destfilesize = filesize($destfilename);
			if ($destfilesize > 0)
			{
				$publicurl = $this->createPublicURL($media);

				if ($publicurl)
				{
					DB::table('medias')
						->where('id', $media->getId())
						->update(array('status' => 'success', 'destfilesize' => $destfilesize, 'publicurl' => $publicurl, 'converted_at' => date('Y-m-d G:i:s')));
					return true;
				}
			}
		}

		DB::table('medias')
			->where('id', $media->getId())
			->update(array('status' => 'error'));
		return false;
	}

	public function isDocument()
	{
		return true;
	}

	public function isVideo()
	{
		return false;
	}
}