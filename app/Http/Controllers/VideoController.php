<?php

namespace App\Http\Controllers;

use App\Media;
use App\MediaType;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class VideoController extends MediaController
{
	public function convert($media, $mode = 'hd')
	{
		$src = array();
		$dest = array();

		// Get some stats on the uploaded video, such as dimensions, aspect ratio, orientation, etc.
		$src['file'] = Config::get('media.temp_dir') . $media->getTempFilename();
		$cmd = Config::get('media.ffmpeg_binary') . ' -i ' . escapeshellarg($src['file']) . ' 2>&1 | egrep -i "rotate"';
		exec($cmd, $output, $status);
		$src['rotate'] = 0;
		$dest['vf'] = '';
		foreach ($output as $out)
		{
			$out = trim(str_replace(' ', '', $out));
			if (is_numeric(stripos($out, 'rotate')))
			{
				$src['rotate'] = substr($out, 7);
		
				// We could also just do a modulus division by 90, but these statements are used to
				// spell it out more clearly.
				switch($src['rotate'])
				{
					case '90':
						$dest['vf'] .= 'transpose=1,';
						break;
					case '180':
						$dest['vf'] .= 'hflip,vflip,';
						break;
					case '270':
						$dest['vf'] .= 'transpose=2,';
						break;
					default:
						break;
				}
			}
		}
		
		// Determine the appropriate width, height, and orientation settings.
		if ($mode == 'hd')
		{
			$dest['width'] = Config::get('media.default_video_conversion_width');
			$dest['height'] = Config::get('media.default_video_conversion_height');
			$dest['crf'] = '22';
			$dest['minrate'] = '2000k';
			$dest['maxrate'] = '3000k';
		}
		else
		{
			$dest['width'] = Config::get('media.default_video_conversion_width');
			$dest['height'] = Config::get('media.default_video_conversion_height');
			$dest['crf'] = '20';
			$dest['minrate'] = '600k';
			$dest['maxrate'] = '1500k';
		}
		
		$dest['vf'] .= 'scale="' . $dest['width'] . ':trunc(ow/a/2)*2",';
		$dest['vf'] = substr($dest['vf'], 0, -1);	// Remove the trailing comma.
		
		// Get the target file path.
		$dest['file'] = Config::get('media.content_dir') . $media->getDestFilename();

		// Convert the video.
		$cmd = Config::get('media.ffmpeg_binary') . ' -threads 4 -y -i ' . escapeshellarg($src['file']) . ' -y -f mp4 -vcodec libx264';
		$cmd .= ' -vf ' . $dest['vf'];
		$cmd .= ' -crf ' . $dest['crf'];
		$cmd .= ' -minrate ' . $dest['minrate'];
		$cmd .= ' -maxrate ' . $dest['maxrate'];
		// $cmd .= ' -bufsize 1500k -preset slow -level 3.0 -strict -2 -acodec aac -ac 2 -ab 192k ';
//		$cmd .= ' -metadata:s:v rotate="0" -bufsize 1500k -level 3.0 -strict -2 -acodec aac -ac 2 -ab 192k ';
		$cmd .= ' -metadata:s:v rotate="0" -bufsize 1500k -level 3.0 -strict -2 -acodec aac -ac 2 -b:a 64k ';
		$cmd .= escapeshellarg($dest['file']);

		echo $cmd;
		exec($cmd);
		
		// Use MP4Box to fix the buffering.
		$cmd = Config::get('media.mp4box_binary') . ' -tmp /tmp -inter 500 ' . escapeshellarg($dest['file']);
		shell_exec($cmd);

		// Make sure a file was created.
		if (!file_exists($dest['file']))
		{
			DB::table('medias')
				->where('id', $media->getId())
				->update(array('status' => 'error'));
			return;
		}

		$destfilesize = filesize($dest['file']);
		if ($destfilesize == 0)
		{
			DB::table('medias')
				->where('id', $media->getId())
				->update(array('status' => 'error'));
			return;
		}		

		$publicurl = $this->createPublicURL($media);
//		echo 'url';

		DB::table('medias')
			->where('id', $media->getId())
			->update(array('status' => 'success', 'destfilesize' => $destfilesize, 'publicurl' => $publicurl, 'converted_at' => date('Y-m-d G:i:s')));
		return;
	}

	public function isDocument()
	{
		return false;
	}

	public function isVideo()
	{
		return true;
	}
}