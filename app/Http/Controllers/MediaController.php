<?php

namespace App\Http\Controllers;

use App\Media;
use App\MediaType;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class MediaController extends Controller
{
	public function moveTempFile($file)
	{
		$extension = strtolower($file->getClientOriginalExtension());
		$tempfile = 'temp_' . uniqid() . '_' . substr(md5(rand(0, 1000)), 0, 8) . '.' . $extension;
//		if ($file->move(Config::get('media.temp_dir') . $tempfile, $file->getClientOriginalName()))
		if ($file->move(Config::get('media.temp_dir'), $tempfile))
			return $tempfile;
		else
			return false;
	}

	public function createMediaRecord($file, $destext)
	{
		$mediatype = new MediaType;
		if ($mediatype->getMimeTypeForExtension($destext) == false)
			return false;

		// If the detected mimetype is "application/zip" but the extension is .docx, assume it's a
		// legitimate DOCX file. It might be an older DOCX spec.
		// TODO: Figure out how to avoid this dumb hack.
		if ($file->getMimeType() == 'application/zip' && strtolower($file->getClientOriginalExtension()) == 'docx')
			$mimetype = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
		else
			$mimetype = $file->getMimeType();
		$tempfile = $this->moveTempFile($file);

		if ($tempfile !== false)
		{
			$uniqueid = substr(md5(rand(0, 1000)), 0, 8) . '_' . uniqid();

			// Create a new media record.
			$media = new Media;
			$media->uniqueid = $uniqueid;
			$media->sessionid = substr(md5(rand(0, 1000)), 0, 16);
			$media->uploadedby = 0;
			$media->srcfilename = $file->getClientOriginalName();
			$media->srcmimetype = $mimetype;
			$media->srcext = strtolower($file->getClientOriginalExtension());
			$media->srcfilesize = filesize(Config::get('media.temp_dir') . $tempfile);

			$media->tempfilename = $tempfile;

			$destfilename = $uniqueid . '.' . $destext;
			$media->destfilename = $destfilename;
			$media->destmimetype = $mediatype->getMimeTypeForExtension($destext);
			$media->destext = $destext;
			$media->destfilesize = 0;

			$media->publicurl = null;

			$media->access = 'public';
			$media->converted_at = null;
			$media->downloaded = 0;
			$media->status = 'pending';

			$media->save();
			$mediaid = $media->id;

			// Initiate conversion process if we successfully saved the record.
			// TODO: Put these into a queue.
			if ($mediaid)
			{
//				$cmd = 'wget -q "' . Config::get('media.base_url') . '/convert/' . $mediaid . '/' . $media->sessionid . '"';
				$cmd = 'curl "' . Config::get('media.base_url') . '/convert/' . $media->getUniqueId() . '/' . $media->getSessionId() . '" > /dev/null 2>/dev/null &';
//				Illuminate\Console\Command::call($cmd);
				@exec($cmd);

				DB::table('medias')
					->where('id', $media->getId())
					->update(array('status' => 'converting'));

				$result = array('id' => $media->getUniqueId(),
								'status' => $media->getStatus(),
								'sessionid' => $media->getSessionId(),
								'publicurl' => Config::get('media.base_url') . '/file/' . $media->getDestFilename(),
								'srcfilename' => $media->getSrcFilename(),
								'srcmimetype' => $media->getSrcMimeType(),
								'srcext' => $media->getSrcExt(),
								'srcfilesize' => $media->getSrcFilesize(),
								'tempfilename' => $media->getTempFilename(),
								'destfilename' => $media->getDestFilename(),
								'destmimetype' => $media->getDestMimeType(),
								'destext' => $media->getDestExt(),
								'destfilesize' => $media->getDestFilesize(),
								'created_at' => time(),
								'updated_at' => time(),
				);

//				echo Config::get('media.base_url') . '/file/' . $media->getDestFilename();
				return $result;
			}
		}
//		return false;
	}

	/**
	 * TODO: This should eventually be an Amazon S3 URL.
	 */
	public function createPublicURL($media)
	{
		return Config::get('media.content_base_url') . $media->getDestFilename();
	}

	public function getDownload($destfilename)
	{
		// There are four possibilities:
		// 1. The file is still converting (pending).
		// 2. The file conversion failed (error).
		// 3. The file conversion succeeded and the file is available for download (success).
		// We could use HTTP status codes to indicate #1 (404), and #2 (400), and a working file for $3.

		// Check the database for a file conversion record.
		$media = Media::where('destfilename', $destfilename)->firstOrFail();
		if ($media->getStatus() == 'pending')
			App::abort('404');
		else if ($media->getStatus() == 'converting')
			App::abort('400');
		else if ($media->getStatus() == 'error')
			App::abort('400');

		$filepath = Config::get('media.content_dir') . $destfilename;
		if (file_exists($filepath))
		{
			$headers = array(
				'Content-Type' => $media->getDestMimeType()
			);
			return response()->download($filepath, $destfilename, $headers);
		}
		return;
	}

	public function getStatus($uniqueid, $returntype = 'text')
	{
		$media = Media::where('uniqueid', $uniqueid)->firstOrFail();
		$result = array('id' => $media->getUniqueId(),
						'file' => $media->getPublicURL(),
						'status' => $media->getStatus());
		if ($returntype == 'json')
			return response()->json($result);
		else
			return response()->make($media->getStatus(), 200, array('Content-Type' => 'text/plain'));
	}
}
