<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MediaType extends Model
{
	var $document_mimetypes = array(
		"doc" => "application/msword",
		"docx" => "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
		"htm" => "text/html",
		"html" => "text/html",
		"odp" => "application/vnd.oasis.opendocument.presentation",
		"ods" => "application/vnd.oasis.opendocument.spreadsheet",
		"odt" => "application/vnd.oasis.opendocument.text",
		"pdf" => "application/pdf",
		"ppt" => "application/vnd.ms-powerpoint",
		"pptx" => "application/vnd.openxmlformats-officedocument.presentationml.presentation",
		"rtf" => "text/rtf",
		"txt" => "text/plain",
		"xls" => "application/vnd.ms-excel",
		"xlsx" => "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
	);

	var $video_mimetypes = array(
		'asf' => 'video/x-ms-asf',
		'avi' => 'video/x-msvideo',
		'divx' => '',
		'flv' => 'video/x-flv',
		'm4v' => '',
		'moov' => '',
		'mov' => 'video/quicktime',
		'mp4' => 'video/mp4',
		'mpeg' => 'video/mpeg',
		'mpg' => 'video/mpeg',
		'rm' => '',
		'rmvb' => '',
		'vob' => '',
		'wmv' => 'video/x-ms-wmv',
		'3gp' => 'video/3gpp',
	);

	public function getMimeTypeForExtension($ext)
	{
		$ext = strtolower($ext);
		if (isset($this->document_mimetypes[$ext]))
		{
			return $this->document_mimetypes[$ext];
		}
		else if (isset($this->video_mimetypes[$ext]))
		{
			return $this->video_mimetypes[$ext];
		}
		return false;
	}

	public function getExtensionForMimeType($mimetype)
	{
		foreach ($this->document_mimetypes as $ext => $mt)
		{
			if ($mt == $mimetype)
				return $ext;
		}

		foreach ($this->video_mimetypes as $ext => $mt)
		{
			if ($mt == $mimetype)
				return $ext;
		}
		return false;
	}

	public function isDocumentByExtension($ext)
	{
		// If the detected mimetype is "application/zip" but the extension is .docx, assume it's a
		// legitimate DOCX file. It might be an older DOCX spec.
		// TODO: Figure out how to avoid this dumb hack.
		if ($ext == 'docx')
			return true;
	}

	public function isDocument($q)
	{
		foreach ($this->document_mimetypes as $ext => $mt)
		{
			if ($q == $ext || $q == $mt)
				return true;
		}
		return false;
	}

	public function isVideo($q)
	{
		foreach ($this->video_mimetypes as $ext => $mt)
		{
			if ($q == $ext || $q == $mt )
				return true;
		}
		return false;
	}	
}
