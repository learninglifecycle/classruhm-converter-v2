<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
	protected $id;
	protected $uniqueid;
	protected $sessionid;
	protected $uploadedby;
	protected $srcfilename;		// The original filename.
	protected $srcmimetype;		// The mimetype of the source file.
	protected $srcext;			// The extension of the source file.
	protected $srcfilesize;
	protected $tempfilename;	// The filename of the temp file.
	protected $destfilename;	// The filename of the converted file.
	protected $destmimetype;	// The mimetype of the destination file.
	protected $destext;			// The extension of the destination file.
	protected $destfilesize;
	protected $publicurl;
	protected $downloaded;		// Number of times downloaded.
	protected $access;			// This can be "public" or "private."
	protected $status;			// Pending, error, success.
	protected $converted_at;

	// We can also have separate models for specific things like source and destination dimension sizes for videos.

	protected $table = 'medias';

	public function isDocument()
	{
		$mediatype = new MediaType;
		return $mediatype->isDocument($this->getSrcMimeType());
	}

	public function isVideo()
	{
		$mediatype = new MediaType;
		return $mediatype->isVideo($this->getSrcMimeType());
	}

	public function getId()
	{
		return $this->getAttribute('id');
	}

	public function getUniqueId()
	{
		return $this->getAttribute('uniqueid');
	}

	public function getSessionId()
	{
		return $this->getAttribute('sessionid');
	}

	public function getSrcFilename()
	{
		return $this->getAttribute('srcfilename');
	}

	public function getSrcMimeType()
	{
		return $this->getAttribute('srcmimetype');
	}

	public function getSrcExt()
	{
		return $this->getAttribute('srcext');
	}

	public function getSrcFilesize()
	{
		return $this->getAttribute('srcfilesize');
	}

	public function getTempFilename()
	{
		return $this->getAttribute('tempfilename');
	}

	public function getDestFilename()
	{
		return $this->getAttribute('destfilename');
	}

	public function getDestMimeType()
	{
		return $this->getAttribute('destmimetype');
	}

	public function getDestExt()
	{
		return $this->getAttribute('destext');
	}

	public function getDestFilesize()
	{
		return $this->getAttribute('destfilesize');
	}

	public function getStatus()
	{
		return $this->getAttribute('status');
	}

	public function getPublicURL()
	{
		return $this->getAttribute('publicurl');
	}

	public function getThumbnailURL()
	{
		
	}
}