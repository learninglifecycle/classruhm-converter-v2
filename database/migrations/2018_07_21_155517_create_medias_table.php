<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medias', function (Blueprint $table) {
			$table->increments('id');
			$table->string('uniqueid');
			$table->string('sessionid')->nullable();
			$table->integer('uploadedby')->default(0);
			$table->string('srcfilename');
			$table->string('srcmimetype');
			$table->string('srcext');
			$table->integer('srcfilesize')->default(0);
			$table->string('tempfilename');
			$table->string('destfilename');
			$table->string('destmimetype');
			$table->string('destext');
			$table->integer('destfilesize')->default(0);
			$table->string('publicurl')->nullable();
			$table->string('access')->default('public');
			$table->string('status')->default('pending');
			$table->integer('downloaded')->default(0);
			$table->timestamp('converted_at')->nullable();

			// Indexes.
			$table->unique('uniqueid');
			$table->index('uploadedby');
			// created_at, updated_at DATETIME
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medias');
    }
}
