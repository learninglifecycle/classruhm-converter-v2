<?php

use App\Http\Controllers\MediaController;
use App\Media;
use App\MediaType;
use Illuminate\Support\Facades\Input;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('upload');
});

Route::get('file/{destfilename}', 'MediaController@getDownload');

Route::get('status/{uniqueid}', 'MediaController@getStatus');

Route::get('status/{uniqueid}/{returntype}', 'MediaController@getStatus');

// TODO: Make sure this route is only called from the command-line.
Route::get('convert/{uniqueid}/{sessionid}', function($uniqueid, $sessionid)
{
	// Determine which controller to use.
	$media = Media::where('uniqueid', $uniqueid)->where('sessionid', $sessionid)->firstOrFail();

	if ($media->isDocument())
	{
		$app = app();
		$doc = $app->make('App\Http\Controllers\DocumentController');
		$doc->callAction('convert', array('media' => $media));
	}
	else if ($media->isVideo())
	{
		$app = app();
		$doc = $app->make('App\Http\Controllers\VideoController');
		$doc->callAction('convert', array('media' => $media));
		
	}
});

Route::post('upload', function()
{
	$files = Input::file();
	$targetext = Input::get('targettype');

	$mediaType = new MediaType;
	foreach ($files as $file)
	{
		// If autodetecting the target extension, videos go to MP4, and documents go to PDF.
		// TODO: Convert PowerPoints to HTML5.
		if (!isset($targetext) || $targetext == 'auto')
		{
			if ($mediaType->isDocument($file->getMimeType()))
			{
				$targetext = 'pdf';
			}

			// If the detected mimetype is "application/zip" but the extension is .docx, assume it's a
			// legitimate DOCX file. It might be an older DOCX spec.
			// TODO: Figure out how to avoid this dumb hack.
			else if ($file->getMimeType() == 'application/zip' && $mediaType->isDocumentByExtension(strtolower($file->getClientOriginalExtension())))
			{
				$targetext = 'pdf';
			}

			else if ($mediaType->isVideo(strtolower($file->getClientOriginalExtension())))
			{
				$targetext = 'mp4';
			}
			else
				App::abort('404');
		}

		// Make sure we have a valid mimetype.
		if ($mediaType->isDocument($file->getMimeType()) || $mediaType->isDocumentByExtension(strtolower($file->getClientOriginalExtension())) || $mediaType->isVideo(strtolower($file->getClientOriginalExtension())))
		{
			$app = app();
			$doc = $app->make('App\Http\Controllers\MediaController');
			$output = $doc->callAction('createMediaRecord', array('file' => $file, 'destext' => $targetext));
			return $output;
		}
//		print_r($file);
	}
});