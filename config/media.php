<?php
$mediaConfig = [
	'base_url' => env('MEDIA_BASE_URL', ''),
	'temp_dir' => env('MEDIA_TEMP_DIR', ''),
	'content_dir' => env('MEDIA_CONTENT_DIR', ''),
	'content_base_url' => env('MEDIA_CONTENT_BASE_URL'),
	'ffmpeg_binary' => env('FFMPEG_BINARY', '/usr/bin/ffmpeg'),
	'mp4box_binary' => env('MP4BOX_BINARY', '/usr/bin/MP4Box'),
	'youtube_binary' => env('YOUTUBE_BINARY', '/usr/local/bin/youtube-dl'),
	'unoconv_binary' => env('UNOCONV_BINARY', '/usr/bin/unoconv'),
	'jod_converter_www' => env('JOD_CONVERTER_WWW', 'http://localhost:8080/converter/service'),
	'default_video_conversion_width' => env('MEDIA_DEFAULT_VIDEO_CONVERSION_WIDTH', 800),
	'default_video_conversion_height' => env('MEDIA_DEFAULT_VIDEO_CONVERSION_HEIGHT', 480),
	'allowed_filetypes' => env('MEDIA_ALLOWED_FILETYPES'),
	'video_filetypes' => env('MEDIA_VIDEO_FILETYPES'),
	'document_filetypes' => env('MEDIA_DOCUMENT_FILETYPES'),
	'document_mimetypes' => [
		"doc" => "application/msword",
		"docx" => "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
		"htm" => "text/html",
		"html" => "text/html",
		"odp" => "application/vnd.oasis.opendocument.presentation",
		"ods" => "application/vnd.oasis.opendocument.spreadsheet",
		"odt" => "application/vnd.oasis.opendocument.text",
		"pdf" => "application/pdf",
		"ppt" => "application/vnd.ms-powerpoint",
		"pptx" => "application/vnd.openxmlformats-officedocument.presentationml.presentation",
		"rtf" => "text/rtf",
		"txt" => "text/plain",
		"xls" => "application/vnd.ms-excel",
		"xlsx" => "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
	],
	'upload_max_filesize' => env('MEDIA_UPLOAD_MAX_FILESIZE', (int) (ini_get('upload_max_size'))),
	'post_max_filesize' => env('MEDIA_POST_MAX_FILESIZE', (int) (ini_get('post_max_size'))),
	'memory_limit' => env('MEDIA_MEMORY_LIMIT', (int) (ini_get('memory_limit'))),
];

$mediaConfig['max_upload'] = min($mediaConfig['upload_max_filesize'], $mediaConfig['post_max_filesize'], $mediaConfig['memory_limit']);

return $mediaConfig;