<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ClassRuhm Media Upload</title>
	<style>
		@import url(//fonts.googleapis.com/css?family=Open+Sans);

		body {
			margin: 0;
			font-family: 'Open Sans', sans-serif;
			color: #999;
		}

		.container {
			width: 600px;
			height: 400px;
			position: absolute;
			left: 50%;
			top: 50%;
			margin-left: -300px;
			margin-top: -200px;
		}

		a, a:visited {
			text-decoration: none;
		}

		h1 {
			font-size: 32px;
			margin: 16px 0 0 0;
		}
	</style>
</head>
<body>
	<div class="container">
		<form action="/upload" method="post" enctype="multipart/form-data">
			<input type="hidden" name="mediatype" value="video" />

			<label for="upload">Video/Document Upload</label>
			<input type="file" id="file" name="sourcefile" />

			<label for="targettype">Target</label>
			<select id="targettype" name="targettype">
				<option value="auto">Autodetect</option>
				<option value="mp4">MP4 (h264)</option>
				<option value="pdf">PDF</option>
			</select>

			<input type="submit" value="Upload" />
		</form>
	</div>
</body>
</html>